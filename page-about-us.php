<?php
/**
 * Template Name: About Us
 * Template Post Type: page
 */

get_header();

pageBanner(
	array(
		'title'    => 'About Us',
		'subtitle' => 'We Are Here to Encourage and Empower!',
	)
);

?>
<!---------------------------------------------------->
<div id="about-us" class="page-section lazyload">
	<div class="wrapper wrapper--no-padding-until-large">
		<div class="row row--gutters-small generic-content-container">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					?>
			<div id="about-page-top" class="row__large-8">
				<div class="about-us  wrapper--b-margin wrapper--gradiant-shadow">

					<h2 class="section-title">Mission Statement</h2>
					<div class="about-us__content">

						<?php the_content(); ?>

					</div>
					<div>
					</div>
				</div>

				<div class="about-us wrapper--b-margin wrapper--gradiant-shadow">
					<div class="about-us__content">
						<h2 class="headline headline--maroon headline--centered headline--padding">Request An Event At
							Your School</h2>
						<?php echo get_field( 'request-an-event' ); ?>
					</div>
					<div>
						<a class="btn btn--maroon btn--large btn--b-margin btn--centered"
							href="<?php echo esc_url( site_url( '/contact-us/?subject=Invite a Speaker to Your Organization' ) ); ?>">Request
							An Event</a>
					</div>
				</div>

				<div class="about-us wrapper--b-margin wrapper--gradiant-shadow">
					<div class="about-us__content">
						<h2 class="headline headline--maroon headline--centered headline--padding">Become a Speaker</h2>
						<?php echo get_field( 'become_a_speaker' ); ?>
					</div>
					<div>
						<a class="btn btn--maroon btn--large btn--b-margin btn--centered"
							href="<?php echo esc_url( site_url( '/contact-us/?subject=Become a Speaker' ) ); ?>">Apply
							Today</a>
					</div>
				</div>
			</div>

			<div class="row__large-4">
				<div class="about-us wrapper--b-margin wrapper--gradiant-shadow">

					<div class="about-us__feature-image">
						<img src="<?php echo esc_url( get_the_post_thumbnail_url( null, 'featureImage' ) ); ?>">

					</div>
					<h2 class="headline headline--maroon headline--centered">The Founders</h2>
					<div class="about-us__side-bar-content">
						<?php echo get_field( 'founders-info' ); ?>
					</div>
				</div>
				<div class="about-us wrapper--b-margin wrapper--gradiant-shadow">
					<h2 class="headline headline--maroon headline--centered headline--padding">Hotline Numbers</h2>
					<div class="about-us__side-bar-content">
						<?php echo get_field( 'hotline_numbers' ); ?>
					</div>
				</div>
			</div>
					<?php
			endwhile;
			endif;
			?>
		</div>
	</div>
</div>
<?php get_footer();
?>
