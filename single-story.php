<?php
  /**
   * Single Stroy
   *
   * Template for the single story page.
   *
   * @category   Components
   * @package    WordPress
   * @subpackage Life Lessons Speaker
   * @author     RFDPrint <sales@rfdprint.com>
   * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
   * @link       https://rfdprint.com
   * @since      1.0.0
   */

get_header();
pageBanner(
	array(
		'title'    => get_the_title(),
		'subtitle' => 'An experience from someone like you.',
	)
);

if ( have_posts() ) {
	the_post();
	?>
<div id="stories" class="page-section lazyload">
	<div class="wrapper wrapper--no-padding-until-large">
		<div class="row row--gutters-small generic-content-container">

			<div class="row__large-8">
				<div class="story  wrapper--gradiant-shadow">
				<div class="story__icon">
						<div class="icon icon--logo-mic"></div>
					</div>
					<h2 class="headline headline--maroon headline--padding-t">Story Number: <?php the_field('story_number') ?></h2>
					<div class="story__content"><?php the_content(); ?></div>
					<div> <a class="btn btn--greyMaroon  btn--t-b-margin"
							href="<?php echo esc_url( get_post_type_archive_link( 'story' ) ); ?>">View All Strories</a>

					</div>
				</div>
			</div>
			<?php

			$category = get_the_category();
			$mainStoryID = get_the_ID();
			if(!empty($category)){$storyCategory = $category[0]->cat_ID;}
			wp_reset_postdata();
}
?>
			<?php
				$args = array(
					'post_type'   => 'story',
					'post_status' => 'publish',
					'cat'    => $storyCategory,
					'posts_per_page' => 3
				);?>
				<div class="row__large-4">
				<h3 class="headline headline--light headline--centered story__similar-stories-headline" >Similar Stories</h3>
				<hr />
				<?php

				$stories = new WP_Query( $args );
				if ( $stories->have_posts() ) :
					?>



				<?php
				$storyDisplayCount= 0;
					while ( $stories->have_posts() && $storyDisplayCount<3 ) :
						$stories->the_post();

				if ($post->ID != $mainStoryID) :
				?>
				<div class="story story--margin-t wrapper--gradiant-shadow">
					<!--<div class="story__icon">
						<div class="icon icon--logo-mic"></div>
					</div> -->
					<h2 class="story__title"><strong><?php echo get_the_title(); ?></strong></h2>
					<p><?php echo get_the_excerpt(); ?></p>
					<a class="btn btn--grey btn--t-b-margin" href="<?php echo esc_url( get_the_permalink( $post->ID ) ); ?>">View Full
						Story</a>
				</div>

							<?php
							$storyDisplayCount++;
				endif;
					endwhile;
					wp_reset_postdata();
					?>

					<?php
					else :
						echo '<h3 class = "story__similar-stories-none"> There are no similar stories </h3>';
endif;
					?>
			</div> <!-- end row__large-4 div -->
		</div> <!-- end row div -->
	</div>

</div>
<?php get_footer(); ?>
<?php
