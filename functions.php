<?php
  /**
   * Functions
   *
   * Main functions.php file for the theme.
   *
   * @category   Components
   * @package    WordPress
   * @subpackage Life Lessons Speaker
   * @author     RFDPrint <sales@rfdprint.com>
   * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
   * @link       https://rfdprint.com
   * @since      1.0.2
   */

   // If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

define( 'EDD_VERSION', '1.0.2' );
/** //TODO !!!DEV MODE!! define( 'EDD_VERSION', microtime()) Defines the script version number !!!DEV MODE!! SET VERSION FOR PRODUCTION*/


/**
 * //TODO add function descrition
 */
function lls_custom_rest() {
	register_rest_field(
		'story',
		'post_type_slug',
		array(
			'get_callback' => function() {
				$obj = get_post_type_object( 'story' );
				return $obj->rewrite['slug'];
			},
		)
	);

}

add_action( 'rest_api_init', 'lls_custom_rest' );


/**
 * //TODO add function description
 */
function life_lessons_speaker_features() {
	register_nav_menu( 'headerMenuLocation', 'Header Menu Location' );
	register_nav_menu( 'footerLocationOne', 'Footer Location One' );
	register_nav_menu( 'footerLocationTwo', 'Footer Location Two' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'featureImage', 300, 300, true );
	add_image_size( 'speakerLandscape', 200, 180, true );
	add_image_size( 'speakerPortrait', 180, 200, true );
	add_image_size( 'speakerSmallPortrait', 90, 100, true );
	add_image_size( 'pageBanner', 1500, 350, true );
}
add_action( 'after_setup_theme', 'life_lessons_speaker_features' );

function story_excerpt_length( $length ) {
	return 18;
}
add_filter( 'excerpt_length', 'story_excerpt_length', 999 );


/**
 * -------------------------------------------------------------
 * Creates a page banner.
 *
 * Creates a pagebanner with a varity of options, including adding a cta overlying
 * a custom background image.
 *
 * @since 1.0.0
 *
 * @param array $args Sets the pagebanner parameters!
 *
 * -------------------------------------------------------------
 */
function pageBanner( $args = null ) {
	/* php header_image()*/

	if ( ! $args['title'] ) {
		$args['title'] = get_the_title();
	}
	if ( ! $args['subtitle'] ) {
		$args['subtitle'] = get_field( 'page_banner_subtitle' );
	}
	if ( ! $args['intro'] ) {
		$args['intro'] = get_field( 'page_banner_introduction' );
	}
	if ( ! $args['showbutton'] ) {
		$args['showbutton'] = false;
	}
	if ( ! $args['ctatext'] ) {
		$args['ctatext'] = 'You can encourage and empower!';
	}
	if ( ! $args['buttontext'] ) {
		$args['buttontext'] = 'Read More';
	}
	if ( ! $args['buttonurl'] ) {
		$args['buttonurl'] = get_site_url();
	}
	if ( ! $args['photo'] ) {
		if ( get_field( 'page_banner_background_image' ) ) {
			$args['photo'] = get_field( 'page_banner_background_image' )['sizes']['pageBanner'];
		} else {
			$args['photo'] = get_theme_file_uri( '/images/lls-page-banner-min.png' );
		}
	}
	?>
<div class="page-banner">
	<div class="page-banner__bg-image"
		style="background-image: url(<?php echo esc_attr( $args['photo'] ); ?>); width:1300;  background-position: center center;"></div>
	<div class="page-banner__text__container wrapper wrapper--padding-large  t-center c-white">
		<h1 class="page-banner__title"><?php echo esc_attr( $args['title'] ); ?></h1>
		<?php if ( $args['subtitle'] ) : ?>
		<div class="page-banner__intro">
			<p><?php echo esc_attr( $args['subtitle'] ); ?></p>
		</div>
		<?php endif ?>
		<?php if ( $args['intro'] ) : ?>
		<p class="page-banner__intro--narrow-justified">
			<?php echo esc_html( $args['intro'] ); ?>
			<?php endif ?>
		</p>
		<?php if ( $args['showbutton'] ) : ?>
		<h3><?php echo esc_html( $args['ctatext'] ); ?></h3>
		<a href="<?php echo esc_url( $args['buttonurl'] ); ?>"
			class="btn btn--maroon"><?php echo esc_html( $args['buttontext'] ); ?></a>
		<?php endif ?>
	</div>
</div>

<?php
}


/**
 * -------------------------------------------------------------
 * Creates a testimonial row block.
 *
 * Creates a testimonial row with a varity of options
 *
 * @since 1.0.0
 *
 * @param array $args Sets the pagebanner parameters!
 *
 * -------------------------------------------------------------
 * */

function lls_testimoninals( $args = null ) {

	if ( ! $args['posts_per_page'] ) {
		$args['posts_per_page'] = -1;
	}

	if ( ! $args['orderby'] ) {
			$args['orderby'] = 'date';
		}

	if ( ! $args['post_id'] ) {
		$args['post_id'] = '';
	}


	$testimonials = new WP_Query(
		array(
			'posts_per_page' => $args['posts_per_page'],
			'post_type'      => 'testimonial',
			'meta_key'       => $args['meta_key'],
			'orderby'        => $args['orderby'],
			'order'          => 'ASC',
			'post__in'       => $args['post_id'], // array expected ie array( 12, 95, 96).
		)
	);

	?>

<div class="row row--equal-height-at-large generic-content-container">
	<div class="wrapper wrapper--no-padding-until-large">
		<div
			class="row row--gutters-extra-small row--equal-height-at-large row--t-padding generic-content-container">
			<?php

			if ( $testimonials->have_posts() ) {
				while ( $testimonials->have_posts() ) {
					$testimonials->the_post();
					?>

				<?php

					$testimonialComments = get_field( 'testimonial_comments' );
					$testimonialStars    = get_field( 'testimonial_stars' );
					$testimonialName     = get_field( 'testimonial_name' );
					$testimonialLocation = get_field( 'testimonial_location' );
					$testimonialVideoID  = get_field( 'testimonial_video_id' );

					switch ( $testimonialStars ) {

						case 0:
							$testimonialStars =
							'<div class="story__icon">
								<div class="icon icon--logo-mic"></div>
							</div>';
							break;
						case 1:
							$testimonialStars = '★';
							break;
						case 2:
							$testimonialStars = '★★';
							break;
						case 3:
							$testimonialStars = '★★★';
							break;
						case 4:
							$testimonialStars = '★★★★';
							break;
						case 5:
							$testimonialStars = '★★★★★';
							break;
					}
					?>
			<div class="row__large-4">
				<div class="testimonial wrapper--gradiant-shadow"

					<?php echo esc_attr( ( $testimonialVideoID ? 'id=video-root data-video=' . $testimonialVideoID : '' ) ); ?>>
					<div class="testimonial__stars"> <?php echo $testimonialStars; ?></div><br>
					<p> <?php echo esc_textarea( $testimonialComments ); ?></p>
					<div class="testimonial__reviewer-container">
						<div class="testimonial__name">- <?php echo esc_textarea( $testimonialName ); ?> </div>
						<div class="testimonial__location"> <?php echo esc_textarea( $testimonialLocation ); ?> </div>
					</div>
				</div>
			</div>
			<?php
			}
		}
		?>
		</div>
	</div>
	<?php wp_reset_postdata(); ?>
</div>
<?php
}


/**
 * //TODO add function descrition
 */
function llsSpeakers( $args = null ) {

	if ( ! $args['posts_per_page'] ) {
		$args['posts_per_page'] = -1;
	}

	if ( ! $args['orderby'] ) {
		$args['orderby'] = 'title';
	}

	if ( ! $args['showbutton'] ) {
		$args['showbutton'] = 'false';
	}

	if ( ! $args['showevents'] ) {
		$args['showevents'] = 'false';
	}

	$allSpeakers = new WP_Query(
		array(
			'posts_per_page' => $args['posts_per_page'],
			'post_type'      => 'speaker',
			'orderby'        => $args['orderby'],
			'order'          => 'ASC',
		)
	);
	?>


<div class="wrapper wrapper--no-padding-until-large">
	<div
		class="row row--gutters row--equal-height-at-large row--gutters row--t-padding generic-content-container">
		<?php
			$speakerCounter == 0;
		if ( $allSpeakers->have_posts() ) {
			while ( $allSpeakers->have_posts() ) {
				$allSpeakers->the_post();
				?>
		<div class="row__large-4
				<?php
				$speakerCounter++;
				if ( $speakerCounter == $allSpeakers->post_count ) {
					echo esc_attr( 'class="speaker--last' );
				};
				?>
			">
			<!--close div row__large-4-->
			<div class="speaker wrapper--gradiant-shadow">
				<div class="speaker__wrapper-upper">
					<div class="speaker__photo">
						<img src="<?php echo esc_url( get_the_post_thumbnail_url( null, 'speakerPortrait' ) ); ?>"
							alt="<?php echo esc_attr( get_the_title() ); ?>">
					</div>
					<h3 class="speaker__title speaker__title--grey">
						<strong><?php echo esc_attr( get_the_title() ); ?></strong></h3>
					<p><?php echo esc_html( get_field( 'speaker_quote' ) ); ?></p>
				</div>
				<div class="speaker__wrapper-lower">
					<?php
					if ( true === $args['showevents'] ) {

						$relatedSpeakers = new WP_Query(
							array(
								'posts_per_page' => -1,
								'post_type'      => 'event',
								'orderby'        => 'title',
								'order'          => 'ASC',
								'meta_query'     => array(
									array(
										'key'     => 'related_speakers',
										'compare' => 'LIKE',
										'value'   => '"' . get_the_ID() . '"',
									),
								),
							)
						);

						if ( $relatedSpeakers->have_posts() ) {
							echo '<hr class="divider--large divider--greyMaroon">';
							echo '<h3 class="speaker__title speaker__title--grey-maroon">Speaker Event(s)</h3>';
							echo '<ul class="min-list">';
							while ( $relatedSpeakers->have_posts() ) {
								$relatedSpeakers->the_post();
								?>
							<li class="speaker__list-item">
								<a href="<?php the_permalink(); ?>">
									<span><?php the_title(); ?></span>
								</a>
								<?php
													if ( ( $relatedSpeakers->current_post + 1 ) != ( $relatedSpeakers->post_count ) ) {
															echo '<hr class="divider--dotted">';
													}
													?>

							</li>
						<?php
							}
							echo '</ul>';
						}
					}
					?>
				</div>
			</div>
		</div>
		<?php
			}
					// End events list
		} else {
			echo '<div class="page-section__no-post" ><h3>There are no speakers posted yet, check back soon.</h3></div><br>';
		}
		?>
	</div>
</div>
<?php if ( true === $args['showbutton'] ) : ?>
<div class="speaker__btn-view-all-wrapper">
	<a class="btn btn--maroon" href="<?php echo esc_url( get_post_type_archive_link( 'speaker' ) ); ?>">View All
		Speakers</a>
</div>
<?php endif ?>
</div>
<?php
		wp_reset_postdata();

}

// Hide the title field on the admin screen for the story post type
add_action( 'admin_init', 'lls_story_post_type_hide_title' );


// populate acf field (sample_field) with post types (sample_post_type)
function acf_populate_story_num_field( $field ) {
	$prevStoryID = get_story_type_last_post_id();

	$prevStoryNumber = get_post_meta( $prevStoryID, 'story_number', true );

	$field['default_value'] = $prevStoryNumber + 1;
	return $field;
}
add_filter( 'acf/load_field/name=story_number', 'acf_populate_story_num_field' );


function get_story_type_last_post_id() {
	$defaults = array(
		'post_type'      => 'story',
		'post_status'    => 'publish',
		'meta_key'       => 'story_number',
		'orderby'        => 'meta_value_num',
		'posts_per_page' => 1,
		'order'          => 'DESC',
	);
	$query    = new WP_Query( $defaults );
	if ( $query->found_posts > 0 ) {
		foreach ( $query->posts as $post ) {
			$values = $post->ID;
		}
	}
	return $values;
}

// generate post title on submit
function auto_title_insert( $value ) {
	global $post;
	$type = get_post_type( $post->ID );

	if ( $type == 'story' ) {
		$lastStoryID = get_story_type_last_post_id();

		$prevValue = get_field( 'story_number', $lastStoryID );
		$value     = $_POST['acf']['field_5ced3a45ab5a2'];

		if ( $value ) {
			$value = 'Story #' . $value;
		} else {
			$nextValue                           = $prevValue + 1;
			$value                               = 'Story #' . $nextValue;
			$_POST['acf']['field_5ced3a45ab5a2'] = $nextValue;
		}
	}

	if ( $type == 'testimonial' ) {
		$testimonialName  = $_POST['acf']['field_5d3b2a386c25d'];
		if ( $testimonialName ) {
			$value = 'Testimonial -' . $testimonialName . ' ID# ' . $post->ID;
		} else {
				$value = 'Testimonial - [Anonymous] ID# ' . $post->ID;
		}
	}
	return $value;
}
  add_filter( 'title_save_pre', 'auto_title_insert' );

/**
 * //TODO add function descrition
 */
function life_lessons_files() {
	wp_enqueue_style( 'life_lessons_main_styles', get_stylesheet_uri(), null, EDD_VERSION );
	wp_enqueue_script( 'app-js', get_template_directory_uri() . '/js/bundled/App-Bundled.js', null, EDD_VERSION, true );
	wp_localize_script(
		'app-js',
		'LifeLessonsSpeakerData',
		array(
			'root_url' => get_site_url(),
			'nonce'    => wp_create_nonce( 'wp_rest' ),
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		)
	);

	wp_localize_script(
		'app-js',
		'ajax_request',
		array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		)
	);

	wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/js/bundled/Vendor-Bundled.js', null, EDD_VERSION, true );
	/*wp_enqueue_script( 'app-jsx', get_template_directory_uri() . '/js/bundled/AppJSX-Bundled.js', 'wp-element', EDD_VERSION, true );*/

}
add_action( 'wp_enqueue_scripts', 'life_lessons_files' );

	require 'fields-groups.php';
	require 'fields-columns.php';
	require ABSPATH . '/mailer.php';
?>
