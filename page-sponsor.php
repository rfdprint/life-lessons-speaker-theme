<?php
/**
 * Template Name: Donation
 * Template Post Type: page
 */

get_header();

pageBanner(
	array(
		'title'      => 'Sponsor Us',
		'showbutton' => false,
		'buttonurl'  => '',
		'ctatext'    => '',
		'subtitle'   => 'We Need Your Help!',
	)
);
?>

<!---------------------------------------------------->
<div id="donation" class="page-section lazyload"
	data-matching-link="#services-link">
	<div class="wrapper wrapper--no-padding-until-large">

		<?php

		if ( have_posts() ) :
		while ( have_posts() ) :
				the_post();
				?>
			<div
				class="row row--gutters row--equal-height-at-large row--gutters-small row--t-padding generic-content-container">

				<div class="row__large-4">
					<div class="donation">
						<div class="donation__content">
							<h2 class="donation__title">Sponsor $100</h2>
							<h3 class="donation__sub-title">Where Do The Funds Go?</h3>
							<hr>
							<ul>
								<li> Get your logo on our website as a donation</li>
								<li>Supports 1 special guest speaker's traveling fee</li>
								<li>Provides a memorable life lesson for our young people</li>
							</ul>
						</div>
						<div class="donation__button">
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="SH6FBBMD2SNJ6">
								<input class="donation__button__image" type="image"
									src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/sponsor-100.png"
									border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
									height="1">
							</form>
						</div>
					</div>
				</div>
				<div class="row__large-4">
					<div class="donation">
						<div class="donation__content">
							<h2 class="donation__title">Sponsor $500</h2>
							<h3 class="donation__sub-title">Where Do The Funds Go?</h3>
							<hr>
							<ul>
								<li>Helps a familiy in need</li>
								<li>Get your logo on our website as a donation</li>
								<li>Provides a speaker for an entire event</li>
								<li>Provides a memorable life lesson for our young people</li>
							</ul>
						</div>
						<div class="donation__button">
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="HV2Y34U9PDS6L">
								<input class="donation__button__image" type="image"
									src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/sponsor-500.png"
									border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
									height="1">
							</form>
						</div>
					</div>
				</div>
				<div class="row__large-4">
					<div class="donation">
						<div class="donation__content">
							<h2 class="donation__title">Sponsor $1000</h2>
							<h3 class="donation__sub-title">Where Do The Funds Go?</h3>
							<hr>
							<ul>
								<li>Helps 2 families in need</li>
								<li>Get your logo on our website as a donation</li>
								<li>Place an ad on our flyer distributed to our schools and seen half of the school year
								</li>
								<li>Provides 2-3 Speakers for events, videos etc.</li>
							</ul>
						</div>
						<div class="donation__button">
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="H29H9LRH7FCLW">
								<input class="donation__button__image" type="image"
									src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/sponsor-1000.png"
									border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
									height="1">
							</form>
						</div>
					</div>
				</div>

				<div class="row__large-4">
					<div class="donation">
						<div class="donation__content">
							<h2 class="donation__title">Sponsor $5000</h2>
							<h3 class="donation__sub-title">Where Do The Funds Go?</h3>
							<hr>
							<ul>
								<li>Helps 5 families in need</li>
								<li>Get your logo on our website as a donation</li>
								<li>Place an ad on our flyer distributed to our schools and seen the entire school year
								</li>
								<li>Leave a personalized 15-30 sec prerecorded message to students</li>
								<li>Provides 4-5 speakers for events, videos etc.</li>
							</ul>
						</div>
						<div class="donation__button">
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="MWQQ9ME97B326">
								<input class="donation__button__image" type="image"
									src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/sponsor-5000.png"
									border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
									height="1">
							</form>
						</div>
					</div>
				</div>

				<div class="row__large-8">
					<div class="donation">
						<div class="donation__content">
							<h2 class="donation__title">Sponsor More</h2>
							<h3 class="donation__sub-title">Where Do The Funds Go?</h3>
							<hr>
							<ul class="donation5000-plus-left">
								<li>Helps 5+ families in need</li>
								<li>Get your logo on our website as a donation</li>
								<li>Memorabilia: Tokens of our appreciation to the students with t-shirts, hats,
									braclets
									etc
								</li>
								<li>Place an ad on our flyer distributed to our schools and seen the entire school year
								</li>
							</ul>
							<ul class="donation5000-plus-right">
								<li>A personalized 15-30 sec prerecorded message to students</li>
								<li>Provides 8-10 Speakers for events, videos and powerful life lessons</li>
								<li>Small % goes to life Lessons Speaker Inc. to help better the lives of students
									everywhere
								</li>
							</ul>
							<hr class="donation-more-border">
						</div>

						<div class="donation__button sposor-options-5000-plus">
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="GXGKJ3HEBY98J">
								<table>
									<tr>
										<td><input type="hidden" name="on0" value="Choose Amount">Choose Amount</td>
									</tr>
									<tr>
										<td><select name="os0">
												<option value="Generous">Generous $7,500.00 USD</option>
												<option value="Amazing">Amazing $10,000.00 USD</option>
												<option value="Wonderful">Wonderful $12,500.00 USD</option>
												<option value="Unreal">Unreal $15,000.00 USD</option>
												<option value="Wow!!!">Wow!!! $20,000.00 USD</option>
											</select> </td>
									</tr>
								</table>
								<input type="hidden" name="currency_code" value="USD">
								<input id="donation__button-5000-plus" type="image" class="donation__button-image-smaller"
									src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/sponsor-5000-plus.png"
									border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
									height="1">
							</form>
						</div>

					</div>
				</div>
			</div>

			<div
				class="row row--gutters row--equal-height-at-large row--gutters-large generic-content-container">
				<div class="row__fill">
					<div class="donation">
						<div class="donation__content">
							<h2 class="donation__title">Sponsor $100 Monthly</h2>
							<h3 class="donation__sub-title">Subscribe To Contribute $100 Each Month</h3>
							<h3 class="donation__sub-title">Where Do The Funds Go?</h3>
							<hr>
							<ul class="donation__monthly-left">
								<li>Get your logo on our website as a donation</li>
								<li>Place an ad on our flyer, every print, each month</li>
								<li>Provides a memorable life lesson for our young people</li>
							</ul>
							<img class="donation__monthly-right"
								src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/classroom-v3.jpg"
								alt="donation Us... We Need Your Help" width="400px" height="200px">
						</div>
						<div class="donation__button">
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="H3BVKN9CVQM7Q">
								<input id="donation__button-100-monthly" type="image"
									class="donation__button-image-smaller btn--left"
									src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/sponsor-100-monthly.png"
									border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
									height="1">
							</form>
						</div>
					</div>

				</div>

				<div class="row__fill">
					<div class="donation">
						<hr>
						<h2 class="donation-title">Sponsor By Check</h2>
						<p class="donation-mail-to"><strong>Mail To: </strong>117 E.Cook St.Libertyville, IL 60048</p>
						<p class="donation-payable"><strong>Make Payable To: </strong>Life Lesson, Inc.</p><br>
						<hr>
					</div>
				</div>

				<div class="row__large-8">
					<div class="donation">

						<h2 class="donation__title">Sonsorship Details</h2>

						<p><strong>Families in Need:</strong> There are so many families at each school that need help with
							food, lunches, dinner, clothing etc.. try to put yourself in their shoes; school is hard
							enough, now ad hunger, stress and personal appearance... lets make them successful.</p>

						<p><strong>Your logo on Our Website:</strong> Your logo will be seen as a proud donation on our
							website.
						</p>

						<p><strong>Supports Speakers:</strong> Valuable life lessons delivered by young adults who have
							experienced
							a life altering circumstance themselves, in hopes of helping others to learn from there life
							choices.
						</p>

						<p><strong>Your Ad on Our Flyer:</strong> Put your ad with an offer on our flyer. Our flyer has
							valuable
							life saving numbers for young adults to reach out for help (Donation of $1,000+).</p>

						<p><strong>Pre-recorded message:</strong> Leave a special message from you personally to relay our
							young
							people out there. Wish you can encourage and empower hundreds of students now you can. Your
							message
							will
							be seen at our assemblies, on our website, social media etc. representing your brand and company
							(Donation of $5,000+).</p>

						<p><strong>Memorabilia</strong>: donationed hats, t-shirts, bracelets, etc. items that make it fun
							for
							kids
							to participate and interact (Donations over $5,000).</p>

						<p><strong>Small% to Life Lessons:</strong> We are a proud non profit company who can't do this
							without
							you,
							please be a donation and help better the lives of students everywhere... This program helps
							parents,
							teachers, students, peers, and communities.</p>

						<p><strong> It takes a village to raise a child. Lets give them a fighting chance so they don't have
								to
								fight. Lets show them people care, lets get passionate... What goes around comes around...
								donation
								today....</strong></p>
					</div>
				</div>
				<div class="row__large-4">
					<div>
						<img class="img-side-bar"
							src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/student-side-bar.png"
							alt="Young People with the Power to Encourage" width="400px" height="200px">
					</div>
				</div>

				<!--  Begin Official PayPal Seal
				<div class="pay-pal-badge-div">
					<p style="text-align: center;">
						<a
							href="https://www.paypal.com/us/verified/pal=info%40lifelessonsspeaker%2ecom" target="_blank"
							rel="noopener"><img src="https://www.paypal.com/en_US/i/icon/verification_seal.gif"
								alt="Official PayPal Seal" border="0" /></a><!-- End Official PayPal Seal
					</p>
				</div> -->
				<?php if ( current_user_can( 'administrator' ) ) { ?>
				<div class="test-button-pay-pal">
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
						<input type="hidden" name="cmd" value="_s-xclick">
						<input type="hidden" name="hosted_button_id" value="GLWQGQDDSGMXN">
						<input type="image" class="donation__button-image-smaller "
							src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sponsor-page/sponsor-test.png"
							border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
						<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
							height="1">
					</form>
				</div>
				<?php } ?>
				<?php
						the_content();
				endwhile;
			else :
				esc_attr_e( 'Sorry, no posts matched your criteria.', 'textdomain' );
				endif;
			?>

		</div>
	</div>
		</div>

	<?php
	get_footer();
	?>
