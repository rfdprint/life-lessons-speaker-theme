<?php
/**
 * Frontpage
 *
 * Template for the Life Lessons Speaker frontpage.
 *
 * @category   Components
 * @package    WordPress
 * @subpackage Life Lessons Speaker
 * @author     RFDPrint <sales@rfdprint.com>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link       https://rfdprint.com
 * @since      a
 */


get_header();

pageBanner(
	array(
		'title'      => 'Our Mission Statement',
		'showbutton' => true,
		'buttonurl'  => site_url( '/about-us' ),
		'ctatext'    => 'We Are Here to Encourage and Empower!',
	)
);

?>
<!---------------------------------------------------->
<div id="" class="page-section page-section--services lazyload">
	<div class="wrapper wrapper--no-padding-until-large">
		<div class="row row--gutters row--equal-height-at-large row--gutters-small row--t-padding generic-content-container">

			<div class="row__large-4">
				<div class="service">
					<div class="service__photo">
						<div class="icon icon--microphone "></div>
					</div>


					<h3 class="service__title">Become a Speaker</h3>
					<p>You will be able to share your story with your peers. The fact that you made mistakes, had
						difficulties and got back on the right track, means that you can help and motivate others to
						overcome difficulties as well. Life Lessons is not a counseling service nor claims to be, but we
						are here to help. Join us today, so you can and encourage and empower!</p>
				</div>
			</div>

			<div class="row__large-4">
				<div class="service">
					<div class="service__photo">
						<div class="icon icon--crowd"></div>
					</div>
					<h4 class="service__title">Request a Speaker</h3>
						<p>Life Lessons will visit your school and share our stories with your students. Your school can
							learn from the example and the experience of our speakers. They can ask questions, get
							advice or
							a listening ear, ready to hear their concerns. Allow your students the opportunity to
							empower
							each other.</p>
				</div>
			</div>

			<div class="row__large-4">
				<div class="service">
					<div class="service__photo">
						<div class="icon icon--sponsor"></div>
					</div>
					<h3 class="service__title">Become a Sponsor</h3>
					<p>We are a proud non-profit company who can't do this without you, please be a sponsor and help
						better the lives of students everywhere... This program helps parents, teachers, students,
						peers, and communities. Your contribution goes towards helping local families in need, providing
						speakers, and allows you to advertise your business.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<!---------------------------------------------------->
<div id="testimonials" class="page-section page-section--lightgrey lazyload">
	<div class="wrapper wrapper--no-padding-until-large wrapper">
		<h2 class="section-title"><strong>Testimonials</strong></h2>
		<div class="row row--equal-height-at-large row--gutters-small generic-content-container">

			<div class="row__large-6">
				<div class="testimonial wrapper--gradiant-shadow">
					<div class="testimonial__video">
						<?php echo do_shortcode( '[embedyt] https://www.youtube.com/watch?v=_gKOhttRFGA[/embedyt]' ); ?>
					</div>
					<div class="testimonial__reviewer-container">
						<div class="testimonial__name">-Megan Goedken</div>
						<div class="testimonial__location">Life Lessons Speaker -Libertyville, IL</div>
					</div>
				</div>
			</div>

			<div class="row__large-6">
				<div class="testimonial wrapper--gradiant-shadow">

				<div class="testimonial__name headline--b-margin-small headline--small-general-content">Principal  (Regional Safe School Program)</div>

					<p>
						"Life Lessons has proven to be a valuable asset to our students, as they are able to see how
						some of the choices they have already made or are on the path towards can have great detriment
						in their lives. This program is a unique opportunity for high schoolers to see the real world
						consequences of some of the negative influences they encounter on a daily basis. Students have
						started questioning whether or not their current life path is truly what they want for
						themselves, based on the experiences of the Life Lesson presenters."
					</p>
					<div class="testimonial__reviewer-container">
						<div class="testimonial__name">-Michael Munda</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!---------------------------------------------------->

<!---------------------------------------------------->
<div id="testimonials" class="page-section page-section--lightgrey lazyload">
	<div class="wrapper wrapper--no-padding-until-large wrapp">
		<div class="row row--equal-height-at-large generic-content-container">
		<?php
	lls_testimoninals(
		array(
			'posts_per_page' => 3,
		)
	);
	?>

		</div>
	</div>
</div>

<!---------------------------------------------------->
<div id="speakers" class="page-section page-section--speakers lazyload row--t-padding">
	<h2 class="section-title section-title--grey"><strong>Recent Speakers</strong></h2>

	<?php
llsSpeakers(
	array(
		'posts_per_page' => 3,
		'orderby'     => 'date',
		'showbutton'  => true,
	)
);
?>
</div>
<!---------------------------------------------------->
<div id="sponsors" class="page-section page-section--white">
	<div class="wrapper wrapper--no-padding-until-large">
		<h2 class="section-title section-title--grey"><strong>Sponsors</strong></h2>
			<div class="row__large-6--center">
				<div class="sponsor t-center">
				<?php echo do_shortcode( '[embedyt]https://youtu.be/NTsLGCnVCiE[/embedyt]' ); ?>
				</div>
			</div>
	</div>
</div>
<!---------------------------------------------------->

<!---------------------------------------------------->
<div id="sponsors" class="page-section page-section--white">
	<div class="wrapper wrapper--no-padding-until-large">
		<div class="row row--gutters-large">

			<div class="row__small-4">
				<div class="sponsor">
					<div class="sponsor__image">
						<div class="icon icon--pizza-house"></div>
					</div>
				</div>
			</div>

			<div class="row__small-4">
				<div class="sponsor">
					<div class="sponsor__image">
						<div class="icon icon--cvs-pharmacy"></div>
					</div>
				</div>
			</div>

			<div class="row__small-4">
				<div class="sponsor">
					<div class="sponsor__image">
						<div class="icon icon--potestas-pizza"></div>
					</div>
				</div>
			</div>

			<div class="row__small-4">
				<div class="sponsor">
					<div class="sponsor__image">
						<div class="icon icon--top-nails"></div>
					</div>
				</div>
			</div>

			<div class="row__small-4">
				<div class="sponsor">
					<div class="sponsor__image">
						<div class="icon icon--zogos"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<?php get_footer();
?>