<?php
  /**
   * Template Name: Storyboard
   * Template Post Type: page
   */


get_header();
pageBanner(
	array(
		'title'    => 'Storyboard',
		'subtitle' => 'Life stories from people with experiences like yours.',
	)
);
?>

<div id="story" class="wrapper wrapper--padding-large lazyload">
	<div class="story__search-fields wrapper wrapper--b-margin wrapper--gradiant-shadow">
		<h2 class="headline headline--maroon ">Search Stories</h2>
		<?php
			$args = array(
				'show_option_all' => __( 'All Categories' ),
				'class'           => 'story__search-fields__categories-dropdown',
			);
			?>
		<?php wp_dropdown_categories( $args ); ?>

		<input type="text" id="search-term" class="story__search-fields__textbox search-term"
			placeholder="What are you looking for?">
	</div>
	<div class="spinner-placeholder"></div>
	<div>
		<div class="wrapper wrapper--no-padding-until-large">
			<div id="story__search-results" class="row row--gutters row--equal-height-at-large row--gutters-small generic-content-container"></div>
		</div>
	</div>
</div>

<?php get_footer();
?>
