<?php
  /**
   * Speaker Archive
   *
   * Template for the speakers page. This page uses llsSpeakers() to displat all
   * Life Lessons Speakers.
   *
   * @category   Components
   * @package    WordPress
   * @subpackage Life Lessons Speaker
   * @author     RFDPrint <sales@rfdprint.com>
   * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
   * @link       https://rfdprint.com
   * @since      1.0.0
   */

get_header();
pageBanner(
	array(
		'title'    => 'Speakers',
		'subtitle' => 'Life Lessons speakers want to encourage and empower.',
	)
);
?>

<div id="speakers" class="page-section page-section--speakers-remove-b-margin">
    <?php
    llsSpeakers(
      array(
        'showevents' => true,
      )
    );
    ?>
</div>

<?php get_footer(); ?>
