<?php
  /**
   * Header
   *
   * Main header file for the theme.
   *
   * @category   Components
   * @package    WordPress
   * @subpackage Life Lessons Speaker
   * @author     RFDPrint <sales@rfdprint.com>
   * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
   * @link       https://rfdprint.com
   * @since      1.0.0
   */

      // If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width-device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/favicon.png" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header class="site-header">
		<div class="wrapper">
			<div class="site-header__logo">
				<a href="<?php echo esc_url( get_site_url() ); ?>"> <div class="icon icon--logo"></div></a>
			</div>

			<div class="site-header__menu-icon">
				<div class="site-header__menu-icon__middle"></div>
			</div>

			<div class="site-header__menu group">
				<nav class="main-navigation site-header__menu-content primary-nav primary-nav--pull-right">
					<?php

					/*
					WordPress Menu
					wp_nav_menu(array(
					'theme_location' => 'headerMenuLocation'
					));
					*/

					?>

					<ul>
						<li
						<?php
						if ( is_page( 'about-us' ) || wp_get_post_parent_id( 0 ) === 11 ) {
							echo 'class="current-menu-item"';}
						?>
							>
							<a href="<?php echo esc_url( site_url( '/about-us' ) ); ?>">About Us</a></li>
							<li
						<?php
						if ( is_page( 'contact-us' ) || wp_get_post_parent_id( 0 ) === 11 ) {
							echo 'class="current-menu-item"';}
						?>
							>
							<a href="<?php echo esc_url( site_url( '/contact-us' ) ); ?>">Contact Us</a></li>
						<li
						<?php
						if ( get_post_type() === 'program' ) {
							echo 'class="current-menu-item"';}
						?>
							>
							<a href="<?php echo esc_url( get_post_type_archive_link( 'event' ) ); ?>">Events</a></li>
						<li
						<?php
						if ( get_post_type() === 'event' ) {
							echo 'class ="current-menu-item"';}
						?>
							>
							<a href="<?php echo esc_url( get_post_type_archive_link( 'speaker' ) ); ?>">Speakers</a></li>
						<li
						<?php
						if ( get_post_type() === 'campus' ) {
							echo 'class="current-menu-item"';}
						?>
							>
							<a href="<?php echo esc_url( get_post_type_archive_link( 'testimonial' ) ); ?>">Testimonials</a>
						</li>
						<li
						<?php
						if ( get_post_type() === 'post' ) {
							echo 'class="current-menu-item"';}
						?>
							>
							<a href="<?php echo esc_url( get_post_type_archive_link( 'story' ) ); ?>">Storyboard</a></li>

							<li
							<?php
							if ( get_post_type() === 'campus' ) {
								echo 'class="current-menu-item"';}
							?>
							>

							<a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Sponsor Us') ) ); ?>">Sponsor Us</a>
						</li>

					</ul>
				</nav>

			</div>
		</div>
	</header>
	<div class="header-waypoint-trigger"></div>
