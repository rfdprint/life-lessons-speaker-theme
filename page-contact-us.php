<?php
/**
 * Template Name: Contact Us
 * Template Post Type: page
 */

get_header();
?>
<!---------------------------------------------------->
<div id="contact-us" class="page-section page-section--padding-t lazyload">
	<div class="wrapper wrapper--no-padding-until-large">
	<h2 class="section-title"><strong>Contact Us</strong></h2>
		<div class="row row--gutters-small generic-content-container">
			<div class="row__large-6--center">
				<div class="contact-us wrapper--b-margin">

					<form action="" id="form" class="form form__contact">
					<label for="subject">Reason for contacting us is to:</label>
						<select id="subject" name="subject">

							<option value="Find Out More About Life Lessons">Find Out More About Life Lessons</option>
							<option value="Invite a Speaker to Your Organization">Invite a Speaker to Your Organization</option>
							<option value="Become a Speaker">Become a Speaker</option>
							<option value="Become a Sponsor">Become a Sponsor</option>
							<option value="Submit a Story">Submit a Story</option>
						</select><br>
						<label for="email">Name:</label>
						<input type="text" name="name" id="name" placeholder="Your Name Here"><br>
						<label for="email">Email:</label>
						<input type="text" name="email" id="email" placeholder="email@example.com"><br>
						<label for="message">Message:</label>
						<textarea name="message" id="message" class="form__message" placeholder="message"></textarea><br>
						<input type="checkbox" name="chbx-policy" id="chbx-policy" value="Read"><span class="chbx-policy-text">I have read the <span><a href="<?php echo esc_url( site_url( '/privacy-policy' ) ); ?>">Privacy Policy</a></span> and agree.</span><br>
						<button name="submit" id="submit" class="form__submit__contact btn--greyMaroon btn btn--large">Send</button>
						<label id="info"></label>
					</form>

					<div class="social-icons social-icons--margin-t-small">
						<a href="https://www.linkedin.com/" class="social-icons__icon"><span
								class="icon icon--linkedin"></span></a>
						<a href="https://www.facebook.com/" class="social-icons__icon"><span
								class="icon icon--facebook"></span></a>
						<a href="mailto:[EMAIL ADDRESS]?Subject=Contact%20From%20Request"
							class="social-icons__icon"><span class="icon icon--mail"></span></a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<span id="my_email_ajax_nonce" data-nonce="<?php echo esc_attr( wp_create_nonce( 'my_email_ajax_nonce' ) ); ?>"></span>

<?php get_footer();
?>