<?php
 /**
  * Default Archive
  *
  * Template for the archive pages.
  *
  * @category   Components
  * @package    WordPress
  * @subpackage Life Lessons Speaker
  * @author     RFDPrint <sales@rfdprint.com>
  * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
  * @link       https://rfdprint.com
  * @since      1.0.0
  */

get_header();

pageBanner(
	array(
		'title'    => get_the_archive_title(),
		'subtitle' => get_the_archive_description(),
	)
);
?>

<div class="container container--narrow page-section">
	<?php
	while ( have_posts() ) {
		the_post();
		?>
	<div class="post-item">
		<h2 class="headline headline--medium headline--post-title"><a href="<?php the_permalink(); ?>">
				<?php the_title(); ?></a></h2>
		<div class="metabox">
			<p>Posted by <?php the_author_posts_link(); ?> on <?php the_time( 'n.j.Y' ); ?> in
				<?php echo esc_attr( get_the_category_list( ',' ) ); ?></p>
		</div>
		<div class="generic-content">
			<?php the_excerpt(); ?>
			<p><a class="btn btn--blue" href="<?php the_permalink(); ?>">Continue Reading</a></p>
		</div>
		<?php
	}

	echo esc_attr( paginate_links() );
	?>

	</div>

	<?php get_footer();

	?>
