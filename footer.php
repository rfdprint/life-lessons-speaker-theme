<?php
  /**
   * Footer
   *
   * Main footer file for the theme.
   *
   * @category   Components
   * @package    WordPress
   * @subpackage Life Lessons Speaker
   * @author     RFDPrint <sales@rfdprint.com>
   * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
   * @link       https://rfdprint.com
   * @since      1.0.0
   */

	// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<footer class="site-footer">
	<div class="site-footer__inner wrapper">
		<div class="group row row--gutters row--gutters-small">
			<div class="site-footer__col-one row__medium-6">
				<h2 class="headline ">Terms of Use</h2>
				<div class="site-footer__content--justify">
					<p>Life Lessons not a counseling service nor does anyone associated with the company claim to be
						certified counselors. Please seek professional help if needed. The use of this site and/or the
						suggestions from any person, speaker, user, administrator etc. is at your own risk. Life Lessons
						Speaker is not responsible of any injury, death or lose of finances as a result of the use or
						suggestions
						from this website.

						If you or anyone of concern has or is in a state of emergency call 911 immediately.
					</p>
				</div>
				<div class="site-footer__copyright">
					<p>Content By Life Lessons Speaker | Website Design By RFDPrint</p>
					<p>© Copyright 2019 Life Lessons Speaker, LLC. | © Copyright 2019 RFDPrint.</p>
				</div>
			</div>

			<div class="site-footer__col-two row__medium-6">
				<h2 class="headline">Subscribe to Life Lessons Speaker</h2>
				<div class="site-footer__content--narrow">
					<p>Your personal information will be kept private and will not be shared with others.
						See our <span><a href="<?php echo esc_url( site_url( '/privacy-policy' ) ); ?>">Privacy Policy</a></span></p>
				</div>
				<form  class="form form__footer">
					<input id="footer-email" class="form__footer-email" type="text">
					<button name="footer-submit" id="footer-submit" class="btn--large">Submit</button><br>
					<label class="form__info__footer"></label>
				</form>
			</div>
			<span id="my_email_ajax_nonce" data-nonce="<?php echo wp_create_nonce( 'my_email_ajax_nonce' ); ?>"></span>

		</div>
</footer>
<?php wp_footer(); ?>
</body>

</html>
