<?php

get_header();?>
<div class="page-banner">
	<div class="page-banner__bg-image">

	</div>
	<!--style="background-image: url(<?php echo esc_url( get_theme_file_uri( 'images/hero-placeholder-1900.png' ) ); ?>);">-->

	<div class="page-banner__content wrapper wrapper--padding-large  t-center c-white">
		<h1  class="page-banner__title">Speaker Page</h1>
		<p id="jsx-test">Life lessons Speaker is designed to encourage and empower kids to have the compassion, empathy, and courage
			to stand up for others. Our goals are to provide resources to help build a kinder world where all kids are
			included, where differences are appreciated and where empathy and compassion are nurtured.</p>

		<a href="<?php echo esc_url( get_post_type_archive_link( 'program' ) ); ?>" class="btn btn--maroon">Read
			More</a>
	</div>
</div>


<?php get_footer();
?>
