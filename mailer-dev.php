<?php
/**
 * Mailer
 *
 * Main footer file for the theme.
 *
 * @category   Components
 * @package    WordPress
 * @subpackage Life Lessons Speaker
 * @author     RFDPrint <sales@rfdprint.com>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link       https://rfdprint.com
 * @since      1.0.0
 */

   // If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
}

/**
 * -------------------------------------------------------------
 * Name:      contact_form_process
 * Purpose:   Send contact form
 * -------------------------------------------------------------
 */
// Only logged in users can access this function use this hook.
add_action( 'wp_ajax_contact_form_process', 'lls_contact_form_process' );
// Non logged in users can access this function use this hook.
add_action( 'wp_ajax_nopriv_contact_form_process', 'lls_contact_form_process' );

// add the action.
add_action( 'wp_mail_failed', 'action_wp_mail_failed', 10, 1 );

/**
 * -------------------------------------------------------------
 * Processes and send mail.
 *
 * Processes ajax requests and sends mail based on data recieved.
 * a custom background image.
 *
 * @since 1.0.0
 *
 *
 * -------------------------------------------------------------
 */
function lls_contact_form_process() {
	check_ajax_referer( 'my_email_ajax_nonce', 'security' );
	global $wpdb; // this is how you get access to the database.

	if ( isset( $_POST['action'] ) && 'contact_form_process' === $_POST['action'] ) {
		$subject = sanitize_text_field( $_POST['subject'] );
		$subject = 'Request To: ' . $subject;
		$name    = sanitize_text_field( $_POST['name'] );
		$email   = sanitize_text_field( $_POST['email'] );
		$message = sanitize_text_field( $_POST['message'] );

		$body = '<p>Note: A task for this request has been automatically created in Asana</p><br>
		<h3><strong>' . $subject . '</strong></h3><br>';

		if ( $name ) {
			$body = $body . '<p><strong>Name: </strong>' . $name . ' </p>';
		};

		if ( $email ) {
			$body = $body . '<p><strong>Email: </strong>' . $email . ' </p>';
		};

		if ( $message ) {
			$body = $body . '<p><strong>Message: </strong>' . $message . ' </p><br>';
		} else {
			$body = $body . '<br>';
		};

		$body = $body . '<p>This message was sent automatically from lifelessonsspeaker.com </p>';

		$data = array(
			'status'  => 'message sent successfully',
			'subject' => $subject,
			'email'   => $email,
			'message' => $message,
		);

		$headers[] = 'Content-Type: text/html; charset=UTF-8';

		wp_mail( $email, $subject, $body, $headers );

			wp_send_json_success( $data );
			wp_die();
	}
	$data = array(
		'status' => 'error, message could not send',
	);
	wp_send_json_success( $data );
	wp_die();
}
