<?php

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Add add css to admin screen:
add_action( 'admin_head', 'custom_admin_css_fonts' );
function custom_admin_css_fonts() {
	echo '<style>
  	.column-testimonial-order {
		width:5%;
		text-align: center;
		}
	.column-speaker_name, .column-story_title {
		width:15%;
		font-weight: 700;
	}
	.column-title {
		width:25%;
	}
  </style>';
}

// Add the story columns to the story post type:
add_filter( 'manage_story_posts_columns', 'set_custom_story_columns' );
function set_custom_story_columns( $columns ) {
	 unset( $columns['title'] );
	 unset( $columns['categories'] );
	 unset( $columns['date'] );


	 $columns['story_title']             = __( 'Story Title', 'your_text_domain' );
	 $columns['excerpt']  = __( 'Excerpt', 'your_text_domain' );
	 $columns['categories']        = __( 'Categories', 'your_text_domain' );
	 $columns['date']              = __( 'Date', 'your_text_domain' );

	 return $columns;
}

// Add the event columns to the event post type:
	add_action( 'manage_story_posts_custom_column', 'custom_story_column', 10, 2 );
	function custom_story_column( $column, $post_id ) {
		switch ( $column ) {

			case 'story_title':
			echo '<a href="'. get_edit_post_link() . '">' . get_the_title();
				break;

			case 'excerpt':
			$story = get_the_content();
			$story = wp_trim_words($story, 18 );
			echo $story;
				break;
			}

		}

// Add the event columns to the event post type:
add_filter( 'manage_event_posts_columns', 'set_custom_event_columns' );
function set_custom_event_columns( $columns ) {
	 unset( $columns['title'] );
	 unset( $columns['categories'] );
	 unset( $columns['date'] );


	 $columns['title']			= __( 'Event Title', 'your_text_domain' );
	 $columns['speaker_name']	= __( 'Speaker(s)', 'your_text_domain' );
	 $columns['categories']		= __( 'Categories', 'your_text_domain' );
	 $columns['date']			= __( 'Date', 'your_text_domain' );

	 return $columns;
}

// Add the event columns to the event post type:
	add_action( 'manage_event_posts_custom_column', 'custom_event_column', 10, 2 );
	function custom_event_column( $column, $post_id ) {
		switch ( $column ) {

			case 'speaker_name':
			//$speaker_id = get_the_ID();
			$relatedSpeakers    = get_field( 'related_speakers' );
				$count = 0;
				$len = count($relatedSpeakers);
				foreach ( $relatedSpeakers as $speaker) {
					$speakerList .= '<a href="' . esc_url( get_edit_post_link( $speaker ) ) . '"><span>' . esc_attr( get_the_title( $speaker ) ) . '</span></a>';

					if ($count < $len - 1) {
						$speakerList .= ', ';
					}
					$count++;
				}
				echo $speakerList;
				break;
				}

		}

// Add the speaker columns to the speaker post type:
add_filter( 'manage_speaker_posts_columns', 'set_custom_speaker_columns' );
function set_custom_speaker_columns( $columns ) {
	 unset( $columns['title'] );
	 unset( $columns['categories'] );
	 unset( $columns['date'] );


	 $columns['speaker_name']             = __( 'Speaker', 'your_text_domain' );
	 $columns['speaker_quote']  = __( 'Quote', 'your_text_domain' );
	 $columns['events']  = __( 'Events', 'your_text_domain' );
	 $columns['categories']        = __( 'Categories', 'your_text_domain' );
	 $columns['date']              = __( 'Date', 'your_text_domain' );

	 return $columns;
}

// Add the speaker columns to the speaker post type:
	add_action( 'manage_speaker_posts_custom_column', 'custom_speaker_column', 10, 2 );
	function custom_speaker_column( $column, $post_id ) {
		switch ( $column ) {

			case 'speaker_name':
			echo '<a href="'. get_edit_post_link() . '">' . get_the_title() . '</a>';
				break;

			case 'speaker_quote':
				echo esc_attr( get_field( 'speaker_quote' ) );
				break;

			case 'events':

				$relatedSpeakers = new WP_Query(
					array(
						'posts_per_page' => -1,
						'post_type'      => 'event',
						'orderby'        => 'title',
						'order'          => 'ASC',
						'meta_query'     => array(
							array(
								'key'     => 'related_speakers',
								'compare' => 'LIKE',
								'value'   => '"' . get_the_ID() . '"',
							),
						),
					)
				);

				if ( $relatedSpeakers->have_posts() ) {

				while ( $relatedSpeakers->have_posts() ) {
						$relatedSpeakers->the_post();

					echo '<ul class="min-list">
							<li class="speaker__list-item">
							<a href="' . esc_url( get_edit_post_link() ) . '"><span>' . esc_attr( get_the_title() ) . '</span></a>';

						if ( ( $relatedSpeakers->current_post + 1 ) != ( $relatedSpeakers->post_count ) ) {
									echo '<hr class="divider--dotted">';
						}
						echo '</li>';
					}
					echo '</ul>';
				}
				wp_reset_postdata();
				break;

		}

	}


// Add the testimonial columns to the testimonial post type:
add_filter( 'manage_testimonial_posts_columns', 'set_custom_testimonial_columns' );
function set_custom_testimonial_columns( $columns ) {
	 unset( $columns['title'] );
	 unset( $columns['categories'] );
	 unset( $columns['date'] );

	 $columns['testimonial-order'] = __( 'Order', 'your_text_domain' );
	 $columns['title']             = __( 'Testimonal', 'your_text_domain' );
	 $columns['testimonial-name']  = __( 'Reviewer', 'your_text_domain' );
	 $columns['categories']        = __( 'Categories', 'your_text_domain' );
	 $columns['date']              = __( 'Date', 'your_text_domain' );

	 return $columns;
}

// Add the testimonial columns to the testimonial post type:
add_action( 'manage_testimonial_posts_custom_column', 'custom_testimonial_column', 10, 2 );
function custom_testimonial_column( $column, $post_id ) {
	switch ( $column ) {

		case 'title':
		echo '<a href="'. get_edit_post_link() . '">' . get_the_title();
			break;

		case 'testimonial-name':
			echo esc_attr( get_field( 'testimonial_name' ) );
			break;

		case 'testimonial-order':
			echo esc_attr( get_field( 'testimonial_order_num' ) );
			break;

	}
}


add_filter( 'manage_edit-testimonial_sortable_columns', 'set_custom_testimonial_sortable_columns' );

function set_custom_testimonial_sortable_columns( $columns ) {
	$columns['testimonial-order'] = 'testimonial-order';

	return $columns;
}

add_action( 'pre_get_posts', 'testimonial_custom_orderby' );

function testimonial_custom_orderby( $query ) {
	if ( ! is_admin() ) {
		 return;
	}

	$orderby = $query->get( 'orderby' );

	if ( 'testimonial-order' == $orderby ) {
		  $query->set( 'meta_key', 'testimonial_order_num' );
		  $query->set( 'orderby', 'meta_value_num' );
	}
}

add_action( 'quick_edit_custom_box', 'display_custom_quickedit_book', 10, 2 );

function display_custom_quickedit_book( $column_name, $post_type ) {
	static $printNonce = true;
	if ( $printNonce ) {
		$printNonce = false;
		wp_nonce_field( get_theme_file_uri(), 'testimonial_edit_nonce' );
	}

	?>
<fieldset class="inline-edit-col-right inline-edit-book">
	<div class="inline-edit-col column-<?php echo $column_name; ?>">
		<label class="inline-edit-group">
			<?php
			switch ( $column_name ) {
				case 'book_author':
					?>
			<span class="title">Author</span><input name="book_author" />
					<?php
					break;
				case 'inprint':
					?>
			<span class="title">In Print</span><input name="inprint" type="checkbox" />
					<?php
					break;
			}
			?>
		</label>
	</div>
</fieldset>
	<?php
}

add_action( 'save_post', 'save_book_meta' );



add_action( 'bulk_edit_custom_box', 'quick_edit_custom_box_testimonial', 10, 2 );
add_action( 'quick_edit_custom_box', 'quick_edit_custom_box_testimonial', 10, 2 );

function quick_edit_custom_box_testimonial( $column_name, $post_type ) {
	 $slug = 'testimonial';
	if ( $slug !== $post_type ) {
		return;
	}

	if ( ! in_array( $column_name, array( 'testimonial-order' ) ) ) {
		return;
	}

	static $printNonce = true;
	if ( $printNonce ) {
		$printNonce = false;
		wp_nonce_field( get_theme_file_uri() , 'testimonial_edit_nonce' );
	}
	?>

<fieldset class="inline-edit-col-right inline-edit-video">
	<div class="inline-edit-col inline-edit-<?php echo $column_name; ?>">
		<label class="inline-edit-group">
			<?php
			switch ( $column_name ) {
				case 'testimonial-order':
					?>
			<span class="title">Order</span>
			<input cols="22" rows="1" name="testimonial-order" autocomplete="off">;
					<?php
					break;
			}
			?>
		</label>
	</div>
</fieldset>
	<?php
}


add_action( 'save_post', 'save_book_meta' );

function save_book_meta( $post_id ) {
	/*
	 in production code, $slug should be set only once in the plugin,
	   preferably as a class property, rather than in each function that needs it.
	 */
	$slug = 'testimonial';
	if ( $slug !== $_POST['post_type'] ) {
		return;
	}
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	$_POST += array( "{$slug}_edit_nonce" => '' );
	if ( ! wp_verify_nonce(
		$_POST[ "{$slug}_edit_nonce" ],
		get_theme_file_uri()
	) ) {
		return;
	}

	if ( isset( $_REQUEST['testimonial-order'] ) ) {
		update_post_meta( $post_id, 'testimonial_order_num', $_REQUEST['testimonial-order'] );
	}

}

/* load script in the footer */
if ( ! function_exists( 'wp_my_admin_enqueue_scripts' ) ) :
	function wp_my_admin_enqueue_scripts( $hook ) {

		if ( 'edit.php' === $hook &&
			isset( $_GET['post_type'] ) &&
			'testimonial' === $_GET['post_type'] ) {

			wp_enqueue_script(
				'my_custom_script', get_theme_file_uri( '/js/admin/admin-edits.js' ),
				false, null, true
			);

		}

	}
	endif;
	add_action( 'admin_enqueue_scripts', 'wp_my_admin_enqueue_scripts' );

	function stat_meta_box( $post_type, $post ) {
		add_meta_box('speaker_edit_likned_events', 'Related Event(s)',
			function(){

				$relatedSpeakers = new WP_Query(
					array(
						'posts_per_page' => -1,
						'post_type'      => 'event',
						'orderby'        => 'title',
						'order'          => 'ASC',
						'meta_query'     => array(
							array(
								'key'     => 'related_speakers',
								'compare' => 'LIKE',
								'value'   => '"' . get_the_ID() . '"',
							),
						),
					)
				);

				echo '<p>Link events from the "<span><a href="' . site_url('/wp-admin/edit.php?post_type=event') .'">Events</a></span>" tab on the "Related Speakers" box</p>';
				echo '<hr class="divider--dotted">';

				if ( $relatedSpeakers->have_posts() ) {

				while ( $relatedSpeakers->have_posts() ) {
						$relatedSpeakers->the_post();

					echo '<ul class="min-list">
							<li class="speaker__list-item">
							<a href="' . esc_url( get_edit_post_link() ) . '"><span>' . esc_attr( get_the_title() ) . '</span></a>';

						if ( ( $relatedSpeakers->current_post + 1 ) != ( $relatedSpeakers->post_count ) ) {
									echo '<hr class="divider--dotted">';
						}
						echo '</li>';
					}
					echo '</ul>';
				}
				wp_reset_postdata();
				}, //end function
			array( 'speaker' ), // blank or list of your post_types
			'advanced' // or 'side'
		);
}

add_action( 'add_meta_boxes', 'stat_meta_box', 10, 2 );
?>
