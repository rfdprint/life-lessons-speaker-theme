<?php
  /**
   * Event Archive
   *
   * Template for the event page.
   *
   * @category   Components
   * @package    WordPress
   * @subpackage Life Lessons Speaker
   * @author     RFDPrint <sales@rfdprint.com>
   * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
   * @link       https://rfdprint.com
   * @since      1.0.0
   */

get_header();
pageBanner(
	array(
		'title'    => 'Events',
		'subtitle' => 'See Our Life Lessons Speakers In Action.',
	)
);
?>

<div id="events" class="page-section page-section--lightgrey lazyload">
	<div class="wrapper wrapper--no-padding-until-large">
		<div class="row row--equal-height-at-large row--gutters-small generic-content-container">
			<?php
				$events = new WP_Query(
					array(
						'posts_per_page' => -1,
						'post_type'      => 'event',
						'orderby'        => 'date',
						'order'          => 'ASC',
					)
				);
				?>
			<?php

			if ( $events->have_posts() ) {
				while ( $events->have_posts() ) {
					$events->the_post();
					?>

						<div class="row__large-4">
							<div class="event wrapper--gradiant-shadow">
								<?php
										$eventVideoUrl      = get_field( 'event_video_url' );
										$eventVideoUrl      = esc_url( $eventVideoUrl );
										$eventTitle         = get_the_title();
										$eventExcerpt       = get_the_excerpt();
										$eventPageButtonUrl = get_permalink();
										$relatedSpeakers    = get_field( 'related_speakers' );

								?>

								<?php echo do_shortcode( '[embedyt]' . $eventVideoUrl . '&width=325&height=180[/embedyt]' ); ?>
								<div class="event__wrapper-upper">
									<h3> <?php echo esc_html( $eventTitle ); ?> </h3>
									<?php
									if ( $relatedSpeakers ) {
										foreach ( $relatedSpeakers as $speaker ) {
											?>
									<h4> <?php echo esc_attr( 'Speaker: ' . get_the_title( $speaker ) ); ?> </h4>
											<?php
										}
									}
									?>
									<p class="event__excerpt"><?php echo esc_html( $eventExcerpt ); ?></p>

								</div>
								<div class="event__wrapper-lower">
									<a class="event__btn event__btn-view-event btn btn--small-text btn--maroon"
										href="<?php echo esc_url( $eventPageButtonUrl ); ?>">View Event</a>
									<a class="event__btn event__btn-view-speaker btn btn--small-text btn--maroon"
										href="<?php echo esc_url( $eventPageButtonUrl ); ?>">View Speaker</a>
								</div>
							</div>
						</div>
					<?php
				}
			}
			?>
		</div>
		<?php wp_reset_postdata(); ?>
	</div>
</div>

<?php get_footer();
?>
