<?php
/**
 * Page
 *
 * Main page template file for the theme.
 *
 * @category   Components
 * @package    WordPress
 * @subpackage Life Lessons Speaker
 * @author     RFDPrint <sales@rfdprint.com>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link       https://rfdprint.com
 * @since      1.0.0
 */

get_header();
pageBanner(
	array(
		'title'    => 'All Program',
		'subtitle' => 'There is something for everyone. Have a look around.',
	)
);


 get_footer();
