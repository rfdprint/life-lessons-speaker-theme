<?php
/**
 * Page
 *
 * Main page template file for the theme.
 *
 * @category   Components
 * @package    WordPress
 * @subpackage Life Lessons Speaker
 * @author     RFDPrint <sales@rfdprint.com>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link       https://rfdprint.com
 * @since      1.0.0
 */

get_header();
pageBanner();
?>

<div class="page-section page-section--services wrapper wrapper--padding-large lazyload">
	<div class="wrapper wrapper--no-padding-until-large">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				the_content();
			}
		}
		?>
</div>
</div>
<?php
get_footer();
?>
