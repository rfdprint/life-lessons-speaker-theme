<?php
  /**
   * Single Event
   *
   * Template for the single event page.
   *
   * @category   Components
   * @package    WordPress
   * @subpackage Life Lessons Speaker
   * @author     RFDPrint <sales@rfdprint.com>
   * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
   * @link       https://rfdprint.com
   * @since      1.0.0
   */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		$eventVideoUrl      = get_field( 'event_video_url' );
		$eventVideoUrl      = esc_url( $eventVideoUrl );
		$eventTitle         = get_the_title();
		$eventExcerpt       = get_the_excerpt();
		$eventPageButtonUrl = get_permalink();
		$relatedSpeakers    = get_field( 'related_speakers' );

		if ( $relatedSpeakers ) {
			$count = '';
			foreach ( $relatedSpeakers as $speaker ) {
				$speakerName = get_the_title( $speaker );
				if ( $count > 0 ) {
					if ( $count < 2 ) {
						$speakerList = $speakerList . ', ';
					} else {
						$speakerList = $speakerList . ' & ';
					}
				}
					$speakerList = $speakerList . $speakerName;
					$count ++;
			};
		}
			pageBanner(
				array(
					'title'    => get_the_title(),
					'subtitle' => $speakerList,
				)
			);
		?>

<div id="events" class="page-section lazyload">
	<div class="wrapper wrapper--no-padding-until-large">
		<div class="row row--equal-height-at-large row--gutters-large generic-content-container">

			<div class="row__large-6">
				<div class="event-detail">
					<h2 class="headline headline--centered">Event Video</h2>
					<?php echo do_shortcode( '[embedyt]' . $eventVideoUrl . '&width=325&height=180[/embedyt]' ); ?>
				</div>
			</div>

			<div class="row__large-6">
				<div class="event-detail">
					<h2 class="headline headline--centered">Event Location</h2>
					<iframe style="border: 0;"
						src="https://www.google.com/maps/embed?pb=!4v1517366440534!6m8!1m7!1sMiCjSz8gIUo_T8QkdUIwxw!2m2!1d42.45400907740846!2d-87.83260399415805!3f341.92994484816563!4f-2.8536589483992003!5f0.7820865974627469"
						width="100%" height="322" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
				</div>
			</div>

		</div>
	</div>
</div>
<hr />
<?php
	}
	?>
<div id="events" class="page-section  lazyload">
	<div class="wrapper wrapper--no-padding-until-large">

		<div class="row row--equal-height-at-large row--gutters-large generic-content-container">
			<div class="row__large-6">
				<div class="event-detail event-detail__speakers ">
					<h2 class="headline headline--centered">Speaker(s)</h2>
					<div class="row--equal-height-at-large row--gutters-small row--b-margin-large-list-view">

						<?php
								$speakerListCount = '';
								foreach ( $relatedSpeakers as $speaker ) {
									?>

							<div
								class=" speaker speaker__list-view <?php echo $speakerListCount > 1 ? 'speaker__list-view--hidden' : ''; ?> ">

								<div class="speaker__list-view-wrapper-left">
									<img class="speaker__photo__list-view"
										src="<?php echo esc_url( get_the_post_thumbnail_url( $speaker, 'speakerSmallPortrait' ) ); ?>">
								</div>
								<div class="speaker__list-view-wrapper-right">
									<p class="speaker__quote__list-view">
										<p><strong> <?php echo esc_attr( get_the_title( $speaker ) ); ?></strong></p>
										<hr />
										<?php echo esc_attr( get_field( 'speaker_quote', $speaker ) ); ?>
									</p>

								</div>


							</div>
							<?php
									$speakerListCount++;
								}

								if ( $speakerListCount > 2 ) {
									?>
							<a href="#">There where <?php echo $speakerListCount; ?> speakers for this
								event.
								View All
								Speaker </a>
							<?php
								}
								?>

					</div>
				</div>
			</div>


		<div class="row__large-6">
			<div class="event-detail event__support-us">
				<h2 class="headline headline--centered">Support Us</h2>
				<div class="row--gutters-small">

					<a class="btn btn--small-text btn--maroon btn--half-width"
					href="<?php echo esc_url( site_url( '/contact-us/?subject=Become a Speaker' ) ); ?>">Become a Speaker</a>
					<a class=" btn btn--small-text btn--maroon btn--half-width"
					href="<?php echo esc_url( get_permalink( get_page_by_title( 'Sponsor Us') ) ) ; ?>">Become a Sponsor</a>

				</div>
			</div>
		</div>
		</div>
		<?php
}

?>
	</div>
</div>
<hr />
</div>

<?php get_footer();
?>