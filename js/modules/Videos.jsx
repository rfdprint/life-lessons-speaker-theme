import React from "react";
import YouTube from "react-youtube";

class SingleVideo extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <YouTube
          videoId={this.props.videoID}
          containerClassName={this.props.classContainerName}
          className={this.props.className}
          opts={this.props.opts}
        />{" "}
      </div>
    );
  }

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }
}

export default SingleVideo;
