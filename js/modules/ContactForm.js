import $ from 'jquery';

class ContactForm {
	constructor() {
		this.form = $('#form'),
			this.submit = $('button#submit'),
			this.form = $('#form'),
			this.subject = $('#subject'),
			this.name = $('#name'),
			this.email = $('#email'),
			this.message = $('#message'),
			this.policy = $('#chbx-policy'),
			this.policyText = $('.chbx-policy-text'),
			this.info = $('#info'),
			this.events();
	}

	events() {
		this.policy.hide();
		this.policyText.hide();
		this.subject.change(this.showPolicyChbx.bind(this));
		this.submit.click(this.submitPreventDefault.bind(this));
		this.getQueryStringSubject();
	}

	showPolicyChbx() {
		if (this.subject.val() === 'Submit a Story') {
			this.policy.show();
			this.policyText.show();
			//this.policyText.slideUp();
		} else {
			this.policy.hide();
			this.policyText.hide();
		}
	}
	getQueryStringSubject() {
		// Construct URL object using current browser URL
		var url = new URL(document.location);

		// Get query parameters object
		var params = url.searchParams;

		// Get value of subjectv
		var subject = params.get("subject");

		// Set it as the dropdown value
		$("#subject").val(subject);
	}

	submitPreventDefault(e) {
		e.preventDefault();
		this.process_send();
	}

	process_send() {
		var that = this;

		if (this.validate(that)) {
			console.log('Process Send');
			this.submit.removeClass('btn--success');
			this.submit.addClass('btn--greyMaroon');
			this.submit.html('SENDING MESSAGE...');
			this.form.on('input', '#email, #subject, #message', function() {
				$(this).css('border-color', '');
				this.info.html('').slideUp();
			});

			var data = {
				'action': 'contact_form_process',
				'name': this.name.val(),
				'email': this.email.val(),
				'subject': this.subject.val(),
				'message': this.message.val(),
				'security': $('#my_email_ajax_nonce').data('nonce'),
			}

			$.ajax({
				url: ajax_request.ajax_url,
				type: 'post',
				data: data,
				cache: false,
				success: function(response) {
					console.log('Got this from the server: ' + response.data.status);
					that.email.val('');
					that.subject.val('');
					that.message.val('');
					that.submit.html('MESSAGE SENT');
					that.submit.addClass('btn--success');
					that.submit.removeClass('btn--greyMaroon');
					that.info.html('Your message has been sent. You will be contacted shortly.').css('color', 'green').slideDown();
					console.log(response.data);
				},
				error: function(err) {
					that.info.html('Sorry, your message could not be sent. Try again later').css('color', 'red').slideDown();
					console.log(err);

					console.log(err.responseText);
					that.submit.removeClass('btn--success');
					that.submit.html('TRY TO SEND AGAIN?');
				}

			});
		}
	};

	validate(that) {
		console.log(that.subject.val());

		var valid = true;
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if (!regex.test(that.email.val())) {
			that.email.css('border-color', 'red');
			valid = false;
		}
		if ($.trim(that.name.val()) === "") {
			that.name.css('border-color', 'red');
			valid = false;
		}
		if ($.trim(that.subject.val()) === "") {
			that.subject.css('border-color', 'red');
			valid = false;
		}
		if ($.trim(that.message.val()) === "") {
			that.message.css('border-color', 'red');
			valid = false;
		}

		if (that.subject.val() === 'Submit a Story') {
			if (!that.policy.prop('checked')) {
				$('.chbx-policy-text').css('color', 'red');
				valid = false;
			}
		}




		return valid;
	}

}

export default ContactForm;
