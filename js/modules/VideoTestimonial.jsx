import $ from "jquery";
import SingleVideo from "./Videos.jsx";
import ReactDOM from "react-dom";

class VideoTestimonial {
  constructor() {
    this.testimonial = $("#video-root");
    this.renderDOMElement();
  }

  renderDOMElement() {
    var videoID = this.testimonial.data("video");
    const opts = {
      height: "195",
      width: "320",
      containerClassName: "video__testimonial__container",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        // <YouTube videoId="kp_w8MIe1_0" opts={opts} onReady={this._onReady} />
        autoplay: 0
      }
    };

    ReactDOM.render(
      <SingleVideo
        className={"Test"}
        containerClassName={"video__testimonial"}
        opts={opts}
        videoID={videoID}
      />,
      document.getElementById("video-root")
    );
  }
}
export default VideoTestimonial;

//.site-header__menu-icon
