import $ from 'jquery';

class StorySearch {
	constructor() {
		/*this.addSearchHTML();*/
		this.resultsDiv = $("#story__search-results");
		this.openButton = $("story__search-btn");
		this.searchCategory = $(".story__search-fields__categories-dropdown");
		this.searchField = $("#search-term");
		this.spinnerPlaceholder = $('.spinner-placeholder');


		this.events();
		this.isOverlayOpen = false;
		this.isSpinnerVisible = false;
		this.previousValue;
		this.typingTimer;
		this.getResults();
	}

	events() {
		$(document).keydown(this.keyPressDispatcher.bind(this));
		this.searchField.keyup(this.typingLogic.bind(this));
		this.searchCategory.change(this.getResults.bind(this));
	}

	typingLogic() {

		if (this.searchField.val() != this.previousValue) {
			clearTimeout(this.typingTimer);

			if (this.searchField.val()) {

				if (!this.isSpinnerVisible) {
					this.spinnerPlaceholder.html('<div class="spinner-loader"</div>');
					this.isSpinnerVisible = true;
				} else {
					this.spinnerPlaceholder.html('');
				}

				this.typingTimer = setTimeout(this.getResults.bind(this), 750);
			} else {
				//this.resultsDiv.html('');
				this.getResults()
				this.isSpinnerVisible = false;
			}


		}

		this.previousValue = this.searchField.val();
	}

	getResults() {
		var $categoryValue = ''
		if (this.searchCategory.val() && 0 != this.searchCategory.val()) {
			$categoryValue = '&categories=' + this.searchCategory.val();
		}


		if (this.searchField.val()) {
			var $searchTextValue = 'search=' + this.searchField.val();
		} else {
			$searchTextValue = '';
		}

		$.getJSON(LifeLessonsSpeakerData.root_url + '/wp-json/wp/v2/story?' + $searchTextValue + $categoryValue, (results) => {
			this.resultsDiv.html(`
            ${results.map(item => `
                        <div class="row__large-4">
                            <div class="story wrapper--gradiant-shadow wrapper--b-margin">
                                <div class="story__icon">
                                    <div class="icon icon--logo-mic"></div>
                                </div>
                                <h2 class="story__title"><strong>${item.title.rendered}</strong></h2>
                                <p>${item.excerpt.rendered}</p>
								<a class="btn btn--grey btn--t-b-margin" href="${LifeLessonsSpeakerData.root_url}/${item.post_type_slug}/${item.slug}">View Full Story</a>
                            </div>
                        </div>`).join('')}
            `);
			this.isSpinnerVisible = false;
		});
	}

	//Code for default wp rest api
	/* $.when(
            $.getJSON(universityData.root_url + '/wp-json/wp/v2/posts?search=' + this.searchField.val()),
            $.getJSON( universityData.root_url + '/wp-json/wp/v2/pages?search=' + this.searchField.val())
        ).then((posts, pages) => {
            var combiedResults = posts[0].concat(pages[0]);
            this.resultsDiv.html(`
            <h2 class="search-overlay__section-title">General Information</h2>

                ${combiedResults.length ? '<ul class="link-list min-list">' : '<p>No general information matches that search</p>'}
                   ${combiedResults.map(item => `<li><a href="${item.link}"> ${item.title.rendered}</a> ${item.type == 'post' ? `by ${item.authorName}` : ''} </li>`).join('')}
                   ${combiedResults.length ? '</ul>' : ''}
           `);
            this.isSpinnerVisible = false;
        }, () => {
            this.resultsDiv.html('<p>Unexpected error; please try again.</p>');
        });
    }*/

	keyPressDispatcher(e) {
		var theKeyPressed = e.keyCode;
		if (theKeyPressed == 83 && !this.isOverlayOpen && !$("input, textarea").is(':focus')) {
			this.openOverlay();
		}
		if (theKeyPressed == 27 && this.isOverlayOpen) {
			this.closeOverlay();
		}

	}

	openOverlay() {
		this.seachOverlay.addClass("search-overlay--active");
		$("body").addClass("body-no-scroll");
		this.searchField.val('');
		setTimeout(() => this.searchField.focus(), 301);
		this.isOverlayOpen = true;
		return false;
	}

	closeOverlay() {
		this.seachOverlay.removeClass("search-overlay--active");
		$("body").removeClass("body-no-scroll");
		this.isOverlayOpen = false;

	}
	/*addSearchHTML() {
		$("body").append(`
    <div class="search-overlay">
    <div class="search-overlay__top">
        <div class="container">
        <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
        <input type="text" id="search-term" class="search-term" placeholder="What are you looking for?">
        <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
        </div>
    </div>

  <div class="container">
    <div id="search-overlay__results"></div>
  </div>
</div>
    `)
	}*/


}
export default StorySearch;
