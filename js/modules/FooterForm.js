import $ from 'jquery';

class FooterForm {
	constructor() {
		this.form = $('.form__footer'),
			this.email = $('#footer-email'),
			this.submit = $('button#footer-submit'),
			this.info = $('.form__info__footer'),
			this.events();
	}

	events() {
		this.submit.click(this.process_send.bind(this));

	}

	submitPreventDefault(e) {
		e.preventDefault();
		console.log(this);
		this.process_send();
	}

	process_send(e) {
		e.preventDefault();
		var that = this;
		if (this.validate(that)) {
			console.log('Process Send');
			this.submit.removeClass('btn--success');
			this.submit.addClass('btn--greyMaroon');
			this.submit.html('SENDING');


			this.form.on('input', '#footer-email', function() {
				$(this).css('border-color', '');
			});

			var data = {
				'action': 'contact_form_process',
				'name': '',
				'email': this.email.val(),
				'subject': 'Add to Mailing List',
				'message': '',
				'security': $('#my_email_ajax_nonce').data('nonce'),
			}

			$.ajax({
				url: ajax_request.ajax_url,
				type: 'post',
				data: data,
				cache: false,
				success: function(response) {
					console.log('Got this from the server: ' + response.data.status);
					that.email.val('');
					that.submit.html('SENT');
					that.submit.addClass('btn--success');
					that.submit.removeClass('btn--greyMaroon');
					that.info.html('Thank You! You have been added to our mailing list.').css('color', 'green').slideDown();
					$("input*").css('border-color', '#ccc');
					console.log(response.data);
					console.log(response.data);
				},
				error: function(err) {
					that.info.html('Sorry, your message could not be sent. Try again later').css('color', 'red').slideDown();
					console.log(err);

					console.log(err.responseText);
					that.submitBtn.removeClass('btn--success');
					that.submitBtn.html('TRY AGAIN?');
				}

			});
		}
	};

	validate(that) {
		//var $email = $('#footer-email');
		var valid = true;
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if (!regex.test(that.email.val())) {
			that.email.css('border-color', 'red');
			valid = false;
		}
		console.log(this.email.val());
		return valid;
	}


}

export default FooterForm;
