import MobileMenu from './modules/MobileMenu';
import StickyHeader from './modules/StickyHeader';
import StorySearch from './modules/StorySearch';
import ContactForm from './modules/ContactForm';
import FooterForm from './modules/FooterForm';

var mobileMenu = new MobileMenu();
var stickyheader = new StickyHeader();
var storysearch = new StorySearch();
var contactform = new ContactForm();
var footerform = new FooterForm();
